<?php
namespace ReputationLoop\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller responsible for rendering pages.
 */
class PageController extends BaseController
{
    /**
     * Home page.
     */
    public function homeAction(Request $request)
    {
        $apiConnector = $this->get('reputation_loop_api_connector');
        $resultsPerPage = 10;

        $data = array(
            'noOfReviews'   =>  $resultsPerPage,
            'internal'      =>  1,
            'google'        =>  1,
            'yelp'          =>  1,
            'offset'        =>  0,
            'threshold'     =>  1,
        );
        $companyDetails = $apiConnector->getCompanyDetails($data);
        $companyDetails = json_decode($companyDetails, true);

        $address = $companyDetails['business_info']['business_address'];
        $coordinates = $this->getCompanyCoordinates($address);

        return $this->render('ReputationLoopCoreBundle:Page:home.html.twig', array(
            'companyDetails'    =>  $companyDetails,
            'coordinates'       =>  $coordinates,
            'page'              =>  0,
            'resultsPerPage'    =>  $resultsPerPage,
        ));
    }

    /**
     * Renders partial template for reviews.
     */
    public function reviewsAction(Request $request)
    {
        $apiConnector = $this->get('reputation_loop_api_connector');

        $page = $request->get('page');
        $resultsPerPage = 10;
        $data = $this->getCompanyDetailRequestData($request, $page, $resultsPerPage);
        $companyDetails = $apiConnector->getCompanyDetails($data);
        $companyDetails = json_decode($companyDetails, true);

        return $this->render('ReputationLoopCoreBundle:Page:reviews.html.twig', array(
            'companyDetails'    =>  $companyDetails,
            'page'              =>  $page,
            'resultsPerPage'    =>  $resultsPerPage,
        ));
    }

    /**
     * Returns array of request data for company details.
     */
    private function getCompanyDetailRequestData(Request $request, $page, $resultsPerPage)
    {
        $noOfReviews = 10;
        $offset = $page * $resultsPerPage;

        return array(
            'noOfReviews'   =>  $noOfReviews,
            'internal'      =>  $request->get('internal'),
            'google'        =>  $request->get('google'),
            'yelp'          =>  $request->get('yelp'),
            'offset'        =>  $offset,
            'threshold'     =>  1,
        );
    }

    /**
     * Returns company location coordinates.
     */
    private function getCompanyCoordinates($address)
    {
        $apcCache = $this->get('apc_cache');
        $geocoder = $this->get('geocoder');

        // Use APC Cache to store coordinates per address.
        $coordinates = $apcCache->fetch($address);
        if (!$coordinates) {
            $coordinates = $geocoder->getCoordinates($address);
            $apcCache->save($address, $coordinates);
        }

        return $coordinates;
    }
}
