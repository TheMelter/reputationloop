<?php
namespace ReputationLoop\CoreBundle\ApiConnector;

class ApiConnector
{
    /**
     * Creates request and returns response based on different methods.
     */
    public function createRequest($url, $method, $data = array())
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = null;

        switch ($method) {
            case ApiMethods::GET:
                $response = $this->handleGETRequest($ch, $url, $data);
                break;
            case ApiMethods::POST:
                $response = $this->handlePOSTRequest($ch, $url, $data);
                break;
            case ApiMethods::PUT:
                $response = $this->handlePUTRequest($ch, $url, $data);
                break;
            case ApiMethods::DELETE:
                $response = $this->handleDELETERequest($ch, $url, $data);
                break;
        }

        return $response;
    }

    /**
     * Handles GET request.
     */
    private function handleGETRequest($ch, $url, $data)
    {
        if (count($data) > 0) {
            $url .= "?" . http_build_query($data);
        }

        curl_setopt($ch, CURLOPT_URL, $url); 
        $response = curl_exec($ch); 
        curl_close($ch);      

        return $response;
    }

    /**
     * Handles POST request.
     */
    private function handlePOSTRequest($ch, $url, $data)
    {
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec($ch); 
        curl_close($ch);      

        return $response;
    }

    /**
     * Handles PUT request.
     */
    private function handlePUTRequest($ch, $url, $data)
    {
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, ApiMethods::PUT);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $response = curl_exec($ch);

        return $response;
    }

    /**
     * Handles DELETE request.
     */
    private function handleDELETERequest($ch, $url, $data)
    {
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, ApiMethods::DELETE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec($ch);

        return $response;
    }
}
