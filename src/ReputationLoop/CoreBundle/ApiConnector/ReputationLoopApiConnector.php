<?php
namespace ReputationLoop\CoreBundle\ApiConnector;

class ReputationLoopApiConnector extends ApiConnector
{
    private $apiKey;
    private $baseUrl;
    private $companyDetailsUrl;

    /**
     * Constructor.
     */
    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
        $this->baseUrl = "http://test.localfeedbackloop.com";
        $this->companyDetailsUrl = $this->baseUrl . "/api";
    }

    /**
     * Returns company details data.
     */
    public function getCompanyDetails($data = array())
    {
        $data = $this->addApiKey($data);

        return $this->createRequest($this->companyDetailsUrl, ApiMethods::GET, $data);
    }

    /**
     * Adds api key to data fields.
     */
    private function addApiKey($data)
    {
        $data['apiKey'] = $this->apiKey;

        return $data;
    }

}
