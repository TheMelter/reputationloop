<?php
namespace ReputationLoop\CoreBundle\ApiConnector;

final class ApiMethods
{
    const GET       =   'GET';
    const POST      =   'POST';
    const DELETE    =   'DELETE';
    const PUT       =   'PUT';
}
