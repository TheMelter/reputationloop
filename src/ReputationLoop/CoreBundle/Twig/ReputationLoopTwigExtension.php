<?php
namespace ReputationLoop\CoreBundle\Twig;

class ReputationLoopTwigExtension extends \Twig_Extension
{
    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        return array(
            'timePassed'    =>  new \Twig_SimpleFilter('timePassed', [$this, 'timePassedFilter']),
            'sourceToIcon'  =>  new \Twig_SimpleFilter('sourceToIcon', [$this, 'sourceToIconFilter']),
            'ratingToStars' =>  new \Twig_SimpleFilter('ratingToStars', [$this, 'ratingToStarsFilter']),
            'reviewUrl'     =>  new \Twig_SimpleFilter('reviewUrl', [$this, 'reviewUrlFilter']),
        );
    }

    /**
     * Returns time passed in a readable format.
     * If precise = true, will return difference in magnitude of at least seconds.
     */
    public function timePassedFilter($dateStr, $precise = false)
    {
        $date = new \DateTime($dateStr);
        $now = new \DateTime('now');

        if (!$precise) {
            $now->setTime(0, 0, 0);
            $date->setTime(0, 0, 0);
        }

        $interval = $now->diff($date);
		$years = $interval->format('%y');
		$months = $interval->format('%m');
        $days = $interval->format('%d');
        $hours = $interval->format('%h');
        $minutes = $interval->format('%i');
        $seconds = $interval->format('%s');
        $sign = $interval->format('%r');

        if ($sign == "-") {
            if ($years > 0) {
                return $years == 1 ? '1 year ago' : "$years years ago";
            }
            if ($months > 0) {
                return $months == 1 ? '1 month ago' : "$months months ago";
            }
            if ($days > 0) {
                if ($days >= 21) {
                    return "3 weeks ago";
                }
                if ($days >= 14) {
                    return "2 weeks ago";
                }
                if ($days >= 7) {
                    return "1 week ago";
                }

                return $days == 1 ? 'Yesterday' : "$days days ago";
            }

            if (!$precise) {
                return "Today";
            }

            if ($hours > 1) {
                return "$hours hours ago";
            }

            if ($hours == 1) {
                return "1 hour ago";
            }
            if ($minutes > 1) {
                return "$minutes minutes ago";
            }
            if ($minutes == 1) {
                return "1 minute ago";
            }
            if ($seconds > 1) {
                return "$seconds seconds ago";
            }
            if ($seconds == 1) {
                return "1 second ago";
            }

            return "Just a moment ago";
        }
        else {
            if ($years > 0) {
                return $years == 1 ? '1 year later' : "$years years later";
            }
            if ($months > 0) {
                return $months == 1 ? '1 month later' : "$months months later";
            }
            if ($days > 0) {
                if ($days >= 21)
                    return "3 weeks later";
                if ($days >= 14)
                    return "2 weeks later";
                if ($days >= 7)
                    return "Next week";
                return $days == 1 ? 'Tomorrow' : "$days days later";
            }
            if ($days == 0) {
                if (!$precise)
                    return "Today";

                return "Just a moment ago";
            }
        }
    }

    /**
     * Turns rating into string of star icons.
     */
    public function ratingToStarsFilter($rating)
    {
        $ratingStr = "";
        $is_floor = false;
        $rating = $this->roundRating($rating);

        if (floor($rating) !== $rating) {
            $rating = floor($rating);
            $is_floor = true;
        }

        for ($i = 1; $i <= 5; $i++) {
            if ($rating >= $i) {
                $ratingStr .= '<i class="icon-star"></i>';
            } elseif ($is_floor) {
                $ratingStr .= '<i class="icon-star-half-alt"></i>';
                $is_floor = false;
            } else {
                $ratingStr .= '<i class="icon-star-empty"></i>';
            }
        }

        return $ratingStr;
    }

    /**
     * Returns icon of source.
     */
    public function sourceToIconFilter($source)
    {
        switch ($source) {
            case 0:
                $icon = '<i class="icon-location"></i> Internal';
                break;
            case 1:
                $icon = '<i class="icon-gplus"></i> Google';
                break;
            case 2:
                $icon = '<i class="icon-yelp"></i> Yelp';
                break;
            default:
                $icon = '<i class="icon-location"></i> Internal';
                break;
        }

        return $icon;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'reputation_loop_extension';
    }

    /**
     * Returns rating rounded to nearest 0.5.
     */
    private function roundRating($rating)
    {
        return round($rating * 2) / 2;
    }

}
