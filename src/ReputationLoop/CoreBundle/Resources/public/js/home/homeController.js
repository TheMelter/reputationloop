var HomeController = {
    init: function() {
        this.filter = $('.filter');
        this.pagination = $('.pagination');
        this.updateUrl = $('.reviews-url').val();
        this.reviewsContainer = $('.reviews');

        this.addEventListeners();
    },

    addEventListeners: function() {

        this.filter.click(function(e) {
            var checked = $(this).data('checked');
            if (checked == "yes") {
                $(this).data('checked', "no");
                $(this).find('i.filter-icon').attr('class', 'icon-check-empty filter-icon');
            } else {
                $(this).data('checked', "yes");
                $(this).find('i.filter-icon').attr('class', 'icon-check filter-icon');
            }

            var page = $('.current-page').val();
            HomeController.updateReviews(page);

            e.preventDefault();
        });

        $('body').on('click', '.pagination', function(e) {
            if ($(this).hasClass('disabled')) {
                return false;
            }
            var page = $(this).data('page');
            HomeController.updateReviews(page);

            e.preventDefault();
        });
    },

    // Update reviews via ajax.
    updateReviews: function(page) {
        var data = HomeController.getRequestData(page);
        var url = HomeController.updateUrl;


        HomeController.reviewsContainer.html('Loading...');
        $.get(url, data, function(response) {
            HomeController.reviewsContainer.html(response);
        });
    },

    // Returns request data needed for company reviews.
    getRequestData: function(page) {
        var data = {};

        // Filter data.
        HomeController.filter.each(function(index) {
            var source = $(this).data('source');
            if ($(this).data('checked') == "yes") {
                data[source] = 1;
            } else {
                data[source] = 0;
            }
        });

        // Pagination data.
        data['page'] = page;

        return data;
    },

    // Updates pagination numbers.
    updatePaginations: function(page) {
        $('.pagination.next').data('page', page + 1);
        $('.pagination.prev').data('page', page - 1);
        $('.current-page').val(page);
    }
};
