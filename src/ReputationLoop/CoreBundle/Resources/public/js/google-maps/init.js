var map;
function initMap() {
    var map_style = [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#e85113"}]},{"featureType":"administrative","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"},{"weight":6}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"administrative.country","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"administrative.country","elementType":"labels.text.fill","stylers":[{"color":"#0095d9"}]},{"featureType":"administrative.country","elementType":"labels.text.stroke","stylers":[{"weight":"4"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"administrative.province","elementType":"labels.text.stroke","stylers":[{"weight":"3"}]},{"featureType":"administrative.locality","elementType":"geometry.stroke","stylers":[{"weight":"1"}]},{"featureType":"administrative.locality","elementType":"labels.text.stroke","stylers":[{"weight":"3"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.stroke","stylers":[{"weight":"2"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.stroke","stylers":[{"weight":"3"}]},{"featureType":"landscape","elementType":"all","stylers":[{"lightness":20},{"color":"#efe9e4"}]},{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#f0e4d3"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"hue":"#11ff00"}]},{"featureType":"poi","elementType":"labels.text.stroke","stylers":[{"lightness":100}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"hue":"#4cff00"},{"saturation":58}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"lightness":-100}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"lightness":100}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#efe9e4"},{"lightness":-25}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efe9e4"},{"lightness":-40}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#efe9e4"},{"lightness":-10}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#efe9e4"},{"lightness":-20}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#19a0d8"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"lightness":-100}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"lightness":100},{"weight":"3"}]},{"featureType":"water","elementType":"labels.icon","stylers":[{"visibility":"off"}]}];

    map_style = [{"featureType":"all","elementType":"all","stylers":[{"invert_lightness":true},{"saturation":10},{"lightness":30},{"gamma":0.5},{"hue":"#435158"}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"color":"#192f2f"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"saturation":"-7"},{"hue":"#00ff6c"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"color":"#1e3233"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"color":"#07949b"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#06bcc6"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#005453"}]}];

    var businessDetails = $('#business-details');
    var businessName = businessDetails.data('name');
    var businessLat = parseFloat(businessDetails.data('lat'));
    var businessLng = parseFloat(businessDetails.data('lng'));

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
        styles: map_style,
        center: {lat: businessLat, lng: businessLng}
    });

    var marker = new google.maps.Marker({
        position: {lat: businessLat, lng: businessLng},
        map: map,
        title: businessName,
    });
}
