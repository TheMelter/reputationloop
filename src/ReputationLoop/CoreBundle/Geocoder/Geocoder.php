<?php
namespace ReputationLoop\CoreBundle\Geocoder;

use ReputationLoop\CoreBundle\ApiConnector\ApiConnector;
use ReputationLoop\CoreBundle\ApiConnector\ApiMethods;

/**
 * Uses Google Maps api in order to find coordinates of address.
 *
 * @author Hyunmin Kim
 */
class Geocoder
{
    private $url;
    private $apiKey;
    private $address;

    private $apiConnector;

    /**
     * Constructor.
     */
    public function __construct(ApiConnector $apiConnector, $apiKey)
    {
        $this->apiConnector = $apiConnector;
        $this->apiKey       = $apiKey;
        $this->url          = "https://maps.googleapis.com/maps/api/geocode/json?";
    }

    /**
     * Returns array of coordinates from specified address.
     */
    public function getCoordinates($address)
    {
        $this->address = $address;
        $data = array(
            'key'       =>  $this->apiKey,
            'address'   =>  $this->address,
        );

        // Make api request.
        $response = $this->apiConnector->createRequest($this->url, ApiMethods::GET, $data);
        $response = json_decode($response, true);

        // If response has more than 1 result, get coordinates of first result.
        $this->results = $response['results'];
        if (count($this->results) > 0) {
            return $this->results[0]['geometry']['location'];
        }

        return null;
    }
}
